package com.example.battleofatlantis;

import android.graphics.Bitmap;

public class Player
{
    int x,y,width, height, HP,MP, ATT,DEF;
    Bitmap AttackButton, GuardButton;

    Player()
    {
        HP = 300;
        MP = 50;
        ATT = 10;
        DEF = 30;
    }


    void TakeAttack(int AttackPower)
    {
        HP-=AttackPower;
    }

    void PlayerGuard()
    {
        HP+=10;
        MP-=5;
        ATT +=4;
        DEF +=5;
    }

    int DealDamage()
    {
        return ATT;
    }

}
